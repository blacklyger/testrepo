Square sq;
int velocity = 10;

void setup(){
  size(700, 700);
  sq = new Square(0, 0);
}

void draw(){
  background(240);
  sq.show();
  sq.check_border();
  delay(1);
}

void keyPressed(){
  switch (keyCode){
    case RIGHT:
      sq.pos.x += velocity;
      break;
    case LEFT:
      sq.pos.x -= velocity;
      break;
    case UP:
      sq.pos.y -= velocity;
      break;
    case DOWN:
      sq.pos.y += velocity;
      break;
  }
}
