from subprocess import run, PIPE
import re
import argparse

output = []

class Argparser:

    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--time", type=str)
    args = parser.parse_args()

def parse(time):

    c = run(["journalctl", "-S", "-" + str(time) + "s"], stdout=PIPE)
    data = c.stdout.decode()

    parsing = re.findall(r"Invalid [\w]+ [\w]+ [\w]+ \d+\.\d+\.\d+\.\d+ [\w]+ \d+", data)

    for x in parsing:
        ports = re.findall(r"port \d+", x)
        ips = re.findall(r"from \d+\.\d+\.\d+\.\d+", x)
        user = re.findall(r"user [\w]+", x)

        port = [p.split(" ")[1] for p in ports]
        ip = [i.split(" ")[1] for i in ips]
        use = [u.split(" ")[1] for u in user]

        p = ''.join(port)
        i = ''.join(ip)
        u = ''.join(use)

        output.append((u, i, p))

    return output

result = parse(Argparser.args.time)
if result:
    for res in result:
        #print(res)
        print("USER: %s , IP: %s , PORT: %s" % (res[0], res[1], res[2]))
else:
    print("No data to parse!")
