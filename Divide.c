#include <stdio.h>

void divide_by(float x, float limit, float precision)
{
	float div = 2.0;

	while (x / div >= limit)
	{
		div = div + precision;
	}

	float result = x / div;
	printf("Input Number: %f\n", x);
	printf("Divided By: %f\n", div);
	printf("Result: %f\n", result);
	if (result < limit)
	{
		printf("Result doesn't exceed: %f\n", limit);
	}
	else
	{
		printf("Result does exceed: %f\n", limit);
	}

}

int main()
{
	
	divide_by(150.0, 10.0, 0.1);
	return 0;
}