import serial
from threading import Thread
import string
import itertools
from time import sleep
import re
import datetime
import os

ser = serial.Serial("/dev/ttyACM0", 19200)

def cmd(command):
    try:
        ser.write(b"%s\r\n" % bytes(command, "utf-8"))
    except Exception as ex:
        print("Error while writing")
        print(ex)

def masscall():
    chars = string.digits
    for x in itertools.product(chars, repeat=4):
        digit = ''.join(x)
        ser.write(b"ATD%s;\r\n" % bytes(digit, "utf-8"))
        sleep(0.1)

def call(inp):
    if inp.startswith("CALL"):
        try:
            caller = inp.split(" ")
            number = caller[1]
            cmd("ATD%s;" % bytes(number, "utf-8"))
        except Exception as d:
            print("Error while Calling!")
            print(d)
    elif inp.startswith("MASSCALL"):
        masscall()
    elif inp == "clear":
        os.system("clear")

def get_incoming_number(arr, counter):
    f = open("Incoming.txt", mode='a')
    inc = []
    time = datetime.datetime.now().strftime("%H:%M:%S - %d.%m.%y")
    try:
        string = arr[1]
        match = re.findall(r"\d+\d+\d+\d+", string)[0]
        print("Incoming call: %s" % match)
        inc.append(match)
        for number in inc:
            f.write("Call %s at: %s" % (counter, time) + " --> " + str(number) + "\n")
    except Exception as b:
        print("Error with incoming call!")
        print(b)

def command():
    try:
        while True:
            i = input("")
            if not i:
                print("No input")
            call(i)
            cmd("%s" % bytes(i, "utf-8"))
    except Exception as e:
        print("Error while writing to the arduino")
        print(e)

def receive():
    count = 0
    try:
        while True:
            out = ser.readline().decode("ISO-8859-1")
            msg = out.strip()
            pro = msg.split(" ")
            for line in pro:
                if line == "":
                    pro.remove(line)
            fi = ' '.join(pro)
            if fi == "RING":
                cmd("ATA")
            elif fi == "ERROR":
                print("NUMBER MAYBE NOT AVAILABLE!")
            elif fi.startswith("+CLIP"):
                get_incoming_number(pro, count)
                count += 1
            print(fi)
    except Exception as f:
        print("Error while reading!")
        print(f)

t = Thread(target=command)
t.start()
t2 = Thread(target=receive)
t2.start()