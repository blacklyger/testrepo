from math import sin, pi
from sys import stdout
from struct import Struct

samp = Struct(">h")

tonleiter = [1, 3, 5, 6, 8, 10, 12, 13]
tons = [1, 3, 5, 6, 8,8, 8,8, 10, 10, 10, 10, 8, 8, 8, 8, 10, 10, 10, 10, 8, 8, 8, 8]

for i in range(int(0.2*48000 * len(tons))):
    value = 20000 * sin(i/48000.0 * 2*pi * 440 * 2**((tons[int(i/9600) % len(tons)] + 2)/12))
    stdout.buffer.write(samp.pack(int(value)))


