import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import psutil
import argparse

class Battery():
    battery = psutil.sensors_battery()
    percentage = int(battery.percent)

class argupar():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--interval", type=int, help="Sets the interval")
    parser.add_argument("-l", "--line-type", type=str, help="Sets the line-type")
    args = parser.parse_args()

fig, ax = plt.subplots()
xdata, ydata = [], []
x2data, y2data = [], []
ln, = plt.plot([], [], argupar.args.line_type)
ln2, = plt.plot([], [], "g-")
plt.legend(["CPU %", "Battery %"])
plt.ylabel("CPU % + Battery %")
plt.title("Live-CPU-Plot")
plt.grid(True)

def init():
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 100)
    return ln,

def update(frame):
    xdata.append(frame)
    ydata.append(psutil.cpu_percent())
    x2data.append(frame)
    y2data.append(Battery.percentage)
    ax.set_xlim(xdata[0], xdata[-1])
    print(xdata[0], xdata[-1])
    ln.set_data(xdata, ydata)
    ln2.set_data(x2data, y2data)

ani = FuncAnimation(fig, update,init_func=init, interval=argupar.args.interval)
plt.show()
