﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitCreep
{
    class Program
    {
        static double divide_by(double x, double limit, double precision = 0.1)
        {
            double div = 2;

            while (x / div >= limit)
            {
                div = div + precision;
            }

            double result = x / div;
            Console.WriteLine("Input Number:" + x);
            Console.WriteLine("Divided By:" + div);
            Console.WriteLine("Result:" + result);
            if (result < limit)
            {
                Console.WriteLine("Result doesn't exceed:" + limit);
            }
            else
            {
                // Shoul actually never happen :)
                Console.WriteLine("Result does exceed:"+ limit);
            }

            return div;
        }


        static void Main(string[] args)
        {
            // Call function
            divide_by(100, 10, 0.1);
            Console.ReadKey();
        }
    }
}