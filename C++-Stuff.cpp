#include <iostream>
#include <string>
#include <math.h>
#include <locale>
#include <chrono>
#include <time.h>

using namespace std;

class Test{

	public:
		Test(string stringi);
		int func(int, int);
		void loopy(int);
		void performance(int);
		void str_things(string);
		int get_size(string);
		void get_current_time();
		double dividing_algorithm(double, double, double);

};

Test::Test(string stringer){ cout << stringer << endl;}

int Test::func(int a, int b){
	return a + b;
}

void Test::loopy(int x){
	long double result;
	for (int i = 0; i < x; i++){
		result = pow(i, 10);
		cout << result << endl;
	}
}

void Test::performance(int y){
	long double result = 0;
	for (int i = 0; i < y; i++){
		result += i;
		cout << result << endl;
	}
}

void Test::str_things(string str){
	locale loc;
	char res[100];
	for (string::size_type i = 0; i < str.length(); i++){
		res[i] = toupper(str[i], loc);
	}
	res[str.length()] = 0;
	cout << res << endl;
}

int Test::get_size(string st){
	int sz = st.length();
	return (int)sz;
}

// Function for getting the current time in C++
// Maybe this isn't the most c++i way ... nvm, it works! :)

void Test::get_current_time(){
	time_t now = time(0);
	struct tm something;
	char res[100];
	something = *localtime(&now);
	strftime(res, sizeof(res), "%X - %d.%m.%Y", &something);
	cout << res << endl;

}

// Implemented my awesome, extremely popular dividing algorithm ... :)

double Test::dividing_algorithm(double x, double limit, double precision){
	double div = 2.0;
	
	while (x / div >= limit){
		div += precision;
	}

	double result = x / div;
	if (result < limit){
		cout << "Doesn't exceed: " << limit << endl;
	}
	else{
		return 1;
	}
	return div;
}

int main(){

	Test tst("Hello World");
	cout << tst.func(10, 20) << endl;
	//tst.loopy(1000);
	tst.performance(100000);
	tst.str_things("Hello World");
	cout << tst.get_size("Hey") << endl;
	tst.get_current_time();
	cout << tst.dividing_algorithm(145.5, 5, 0.01) << endl;
}
