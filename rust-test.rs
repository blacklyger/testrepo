use std::thread;
use std::time::Duration;

fn main(){

    // Data Types
    
    let fl = 1.1;
    let a = 20;
    let x: i8 = 10;
    let tup: (i8, i32, i32) = (10, 1024, 10024);
    let (x, y, z) = tup;
    let arr = [1, 2, 3, 4];
    let my_arr: [i32; 3] = [1024, 2048, 4096];
    println!("{}, {}", tup.0, tup.1);
    println!("{}", my_arr[1]);
    let result: i32 = other_func(1024, 1024);
    println!("{}", result);
    let checker: bool = check(200);
    println!("{}", checker);
    let lify: i8 = lif(true);
    println!("{}", lify);
    looper();
    strings();
    threading();
}

fn looper(){
    let mut counter = 0;
    let loopy = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2;
        }
    };
    println!("LOOPY: {}", loopy);

    let mut number = 10;

    while number != 3 {
        println!("{}", number);

        number -= 1;
    }

    let array = [1, 2, 3, 4, 5];
    for elem in array.iter() {
        println!("{}", elem)
    }

    for elem in (2..40).rev() {
        println!("ELEM: {}", elem);
    }
    
    for i in (1..10) {
        println!("{}", i);
    }
}

fn threading(){
    thread::spawn(|| {
        for i in (1..2){
            println!("THREAD: {}", i);
            thread::sleep(Duration::from_millis(1));
        }
        }
    );

    thread::sleep(Duration::from_millis(1));
}

fn strings(){
    let mut my_string = String::from("Hey");
    my_string.push_str(", world!");
    let str_len = my_string.len();
    println!("MY_STRING: {}", my_string);
    println!("STRING LEN: {}", str_len);
}

fn lif(cond: bool) -> i8{
    let x = if cond {
        10
    } else {
       0
    };

    x
}

fn other_func(x: i32, y: i32) -> i32{
    let result = x + y;
    result
}

fn check(val: i32) -> bool {
    if val > 100 {
        return true;
    }
    else if val < 100 {
        return false;
    }
    else {
        return false;
    }
}
