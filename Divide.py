
# "Dividing algorithm in Python"
def divide_by(x: float, limit: float, precision: float = 0.1):
    div = 2.0
    limit = limit

    while x / div >= limit:
        div += precision

    result = x / div
    print("Input number:", x)
    print("Divided by:", div)
    print("Result:", result)
    if result < limit:
        print("Result does not exceed:", limit)
    else:
        print("Result does exceed:", limit)

divide_by(x=145.5, limit=5, precision=0.01)
