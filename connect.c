#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>

int ser_port = open("/dev/ttyACM0", O_RDWR);

if (ser_port < 0){
	puts("Error %i from open%s\n", errno, strerror(errno));
}

struct termios tty;
memset(&tty, 0, sizeof tty);

if (tcgetattr(ser_port, &tty) != 0){
	puts("Error %i from tcgetattr:%s\n", errno, strerror(errno));
}

tty.c_cflag &= ~PARENB;
//tty.c_cflag |= PARENB;
tty.c_cflag &= ~CSTOPB;
//tty.c_cflag |= CSTOPB;
tty.c_flag |= CS8;
tty.c_cflag &= ~CRTSCTS;
tty.c_cflag |= CREAD | CLOCAL;
tty.c_cflag &= ~ICANON;
tty.c_cflag &= ~ECHO;
tty.c_cflag &= ~ECHOE;
tty.c_cflag &= ~ECHONL;
tty.c_cflag &= ~ISIG;

tty.c_iflag &= ~(IXON | IXOFF | IXANY);
tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);

tty.c_oflag &= ~OPOST;
tty.c_oflag &= ~ONLCR;

// tty.c_cc[VTIME] = 10;
// tty.c_cc[VMIN] = 0;

cfsetispeed(&tty, B9600);
cfsetospeed(&tty, B9600);

if (tcsetattr(ser_port, TCSANOW, &tty) != 0){
	puts("Error %i from tcsetattr: %s\n", errno, strerror(errno));
}

// Message
unsigned char msg[] = {'H', 'e', 'l', 'l', 'o', '\r'};
write(ser_port, "Hello, world!", sizeof(msg));

close(ser_port);
