#include <stdio.h>
#include <iostream>

using namespace std;


// Creating class "testcpp" with its functions
class Rect {
	int width, heigth;
public:
	Rect();
	Rect (int, int);
	int adder();
};

// Declaring functions of the class "testcpp"

// Overloading constuctors
Rect::Rect(){
	width = 10;
	heigth = 100;
}

Rect::Rect (int a, int b){
	width = a;
	heigth = b;
}

int Rect::adder(){
	return (width * heigth);
}

// Main function
int main(){

	Rect tst (10, 20);
	Rect txt;
	cout << tst.adder() << endl;
	cout << txt.adder() << endl;
}