# Little Julia cheat-sheet
# For more information have a look at the official Julia language documentation
# DOC: https://docs.julialang.org/en/v1/

import Base
using Sockets

# Functions
function f(x,y)
	println(x + y)
end
f(10, 10)

# Variables (int, strings, etc.) examples

string  = "Hello World"
new_string = Base.replace(string, "Hello" => "Bye")

if Base.startswith(new_string, "Bye")
	println("String starts with 'Bye'")
end

# Mathematical Operations etc.

# Random numbers
println(Base.rand(1:200, 10))

for rnd_num in Base.rand(1:20, 10)
	println(rnd_num)
end

root = Base.sqrt(144)
println(root)

# Arrays and iterating over them

xy = ["Hey", "ok?", "bye"]
z = [1,2,3]

for value in xy
	println(Base.uppercase(value))
end

println(z)

# Sockets

# Get IP address of a host
println(Sockets.getalladdrinfo("github.com")[1])

# Simple TCP-Client (send string)

function Connect_Send(msg)
	sock = Sockets.connect("host", 1337) # connect[string::host, int::port]
	Sockets.write(sock, msg)
end

Connect_Send("Hello World")

# Simple "UDP - Server" (receiving and printing the data)

function Server(ip, port)
	sock = Sockets.UDPSocket()
	Sockets.bind(sock, ip, port)
	while true
		data = Sockets.recv(sock)
		println(String(data))
	end
end

Server(ip"127.0.0.1", 1337)