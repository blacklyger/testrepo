from tkinter.ttk import Button, Entry, Label
from tkinter import Tk
from MAReplacement import *

class Window(Tk):

    def __init__(self):
        super().__init__()
        self.window()

    def window(self):

        self.button = Button(text="Encrypt / Decrypt", command=self.klick, width=20)
        self.button.pack(padx=40, pady=20, ipadx=50, ipady=30)

        self.text = Entry(width=50)
        self.text.pack()

        self.final = Label()
        self.final.config(width=200, font=("Arial", 20))
        self.final.pack(padx=10, pady=20)

        self.final2 = Label()
        self.final2.config(width=200, font=("Arial", 20))
        self.final2.pack(padx=10, pady=2)

        self.resizable(False, False)
        self.geometry("700x300")
        self.title("Monoalphabetic Substitution")

    def klick(self):
        value = feed(self.text.get())
        self.final2["text"] = "Original: " + self.text.get()
        self.text.delete(0, "")
        self.text.insert(0, value)
        self.final["text"] = "Encoded: " + str(value)

app = Window()
app.mainloop()