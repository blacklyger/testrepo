// C Code using Python's C API
// Math functions implemented
// Maybe it's readable? No it's not (it's C CODE!1!1!)
// Anyways, it works!

#include <Python.h>

int adding(int x, int y){
	return x + y;
}

double powering(int x, int y){
	double result = 1.0;
	for (int i = 1; i < y; i++){
		result = x * result; 
	}
	return result;
}

int fac(int x){
	int result = 1;
	for (int i = 2; i < x + 1; i++){
		result = result * i;
	}
	return result;
}

long int function(long int x){
	long int result = 3;
	for (long int i = 0; i < x; i++){
		result = result * x;
	}

	return result;
}

int bl(int value){
	return value;
}

static PyObject* blubb(PyObject* self, PyObject* args){
	int value;

	if (!PyArg_ParseTuple(args, "i", &value)){
		return NULL;
	}

	if (PyLong_Check(args)){
		PyErr_SetString(PyExc_TypeError, "Wrong argument!");
		return NULL;
	}

	if (value == 10){
		PyErr_SetString(PyExc_ValueError, "WOW! Ten!");
		return NULL;
	}

	if (value < 0){
		return Py_BuildValue("s", "Negative numbers aren't allowed!");
	}

	return Py_BuildValue("i", bl(value));
		
}

static PyObject* func(PyObject* self, PyObject* args){
	long int x = 0;

	if (!PyArg_ParseTuple(args, "l", &x)){
		return NULL;
	}

	if (x < 0){
		PyErr_SetString(PyExc_ValueError, "Negative numbers aren't allowed!");
		return NULL;
	}

	return Py_BuildValue("l", function(x));
}

static PyObject* factorial(PyObject* self, PyObject* args){
	int x;

	if (!PyArg_ParseTuple(args, "i", &x)){
		return NULL;
	}

	return Py_BuildValue("i", fac(x));
}

static PyObject* power(PyObject* self, PyObject* args){
	int x;
	int y;

	if (!PyArg_ParseTuple(args, "ii", &x, &y)){
		return NULL;
	}

	return Py_BuildValue("d", powering(x, y));
}

static PyObject* add(PyObject* self, PyObject* args){
	int x;
	int y;

	if (!PyArg_ParseTuple(args, "ii", &x, &y)){
		return NULL;
	}

	return Py_BuildValue("i", adding(x, y));
}

static PyObject* version(PyObject* self){
	return Py_BuildValue("s", "Version 1.0");
}

static PyMethodDef myMethods[] = {
	{"add", add, METH_VARARGS, "Adding two integers together."},
	{"factorial", factorial, METH_VARARGS, "Return factorial of x."},
	{"power", power, METH_VARARGS, "Returns power of x and y."},
	{"blubb", blubb, METH_VARARGS, "Returns a long."},
	{"func", func, METH_VARARGS, "Returns something :)."},
	{"version", (PyCFunction)version, METH_NOARGS, "returns version."},
	{NULL, NULL , 0, NULL}
};

static struct PyModuleDef myModule = {
	PyModuleDef_HEAD_INIT,
	"myModule",
	"Functions to do mathematical things!",
	-1,
	myMethods
};

PyMODINIT_FUNC PyInit_myModule(void){
	return PyModule_Create(&myModule);
}
