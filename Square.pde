class Square{
  int value;
  PVector position;
  PVector pos;
  
  Square(int x, int y){
      if (random(1) < 0.5){
        value = 2;
      }
      else{
        value = 4;
      }
      
      position = new PVector(x, y);
      pos = new PVector(x + (x + 1), y + (y + 1));
  }
  
  public void show(){
    textSize(50);
    fill(200, 100, 0);
    noStroke();
    rect(pos.x, pos.y, 200, 200);
    fill(255);
    textAlign(CENTER, CENTER);
    text(value, pos.x + 100, pos.y + 100);
  }
  
  public void move_diagonal(int speed){
      pos.x += speed;
      pos.y += speed;
      println(pos.x, pos.y);
      if (pos.x + 180 > width && pos.y + 180 > height){
        pos.x = 0;
        pos.y = 0;
      }
      // Debugging
      else if (pos.x != pos.y){
        pos.x = 0;
        pos.y = 0;
        println("Not equal!");
      }
  }
  
  public void move_random(){
    float random_width = random(1) * width;
    println(random_width);
    
    pos.x = (int) random_width;
    pos.y = (int) random_width;
    
  }
  
  public void check_border(){
    if (pos.x + 150 > width && pos.y + 150 > height){
      pos.x = 0;
      pos.y = 0;
    }
  }
}
