k = "123"
class Lower():
    map = {
        "a": "z", "b": "y", "c": "x",
        "d": "w", "e": "v", "f": "u",
        "g": "t", "h": "s", "i": "r",
        "j": "q", "k": "p", "l": "o",
        "m": "n", "n": "m", "o": "l",
        "p": "k", "q": "j", "r": "i",
        "s": "h", "t": "g", "u": "f",
        "v": "e", "w": "d", "x": "c",
        "y": "b", "z": "a"
    }

class Upper():
    map = {
        "A": "Z", "B": "Y", "C": "X",
        "D": "W", "E": "V", "F": "U",
        "G": "T", "H": "S", "I": "R",
        "J": "Q", "K": "P", "L": "O",
        "M": "N", "N": "M", "O": "L",
        "P": "K", "Q": "J", "R": "I",
        "S": "H", "T": "G", "U": "F",
        "V": "E", "W": "D", "X": "C",
        "Y": "B", "Z": "A"
    }

class Punctuation():
    map = {
        ",": ".", ".": ",", "[" : "{",
        "]": "}", "{": "[", "}": "]",
        "/": "\\", "?": "!", "!": "?",
        "=": "ß", "ß": "=", "\"": "\'",
        "(": "^", ")": "°", " ": " ",
        "1": "9", "2": "8", "3": "7",
        "4": "6", "5": "5", "6": "4",
        "7": "3", "8": "2", "9": "1",
        "\'": "\"", "^": "(", "°": ")"
    }

def feed(plaintext):
    l = len(plaintext)
    encoded = []
    spl = list(plaintext)
    for character in spl:
        if str(character).isupper():
            encoded.append(Upper.map[character])
        elif str(character).islower():
            encoded.append(Lower.map[character])
        elif str(character).isprintable():
            encoded.append(Punctuation.map[character])

    final = ''.join(encoded)
    return final